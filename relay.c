/***************************************************************************
* 
* Project           			        :  shakti devt board
* Name of the file	     		        :  realy.c
* Created date			                :  20.08.2019
* Name of Author               		    :  Soutrick Roy Chowdhury
* Email ID                       	    :  soutrick97@gmail.com
*
*****************************************************************************/


#include "platform.h"
#include "gpio.h"

extern void DelayLoop(unsigned long cntr1, unsigned long cntr2);

void main()
{
    write_word(GPIO_DIRECTION_CNTRL_REG, 0x00000001);
    
    while (1)
    {
        write_word(GPIO_DATA_REG, 0x00000001);
        DelayLoop(1000,5000);
        write_word(GPIO_DATA_REG, 0x00);
        DelayLoop(1000,5000);
        printf("\nWORKING");
    }
    
}
